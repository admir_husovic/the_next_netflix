
# GET STARTED

1. `yarn install`
2. `yarn start`
3. `yarn run test`

To build: 
`yarn build`




# TODO
- [x] add small responsive css. 
- [x] handle config variables.
- [ ] handle errors with message.
- [ ] scalable styling structure.
- [ ] better config variables solution.
- [ ] try using Context to avoid passing props everywhere.
- [ ] add detailed movie view. 
- [ ] add detailed actor view with 10 most popular movies. 

## Search
- [ ] pagination
- [ ] allow adult search parameter with moviedb
- [ ] throttle search requests

## TEST 

- [ ] shallow render tests
  - [ ] test render with mock props to movieListItem: 

## API Resources 
search movies: https://developers.themoviedb.org/3/search/search-movies
movie: https://developers.themoviedb.org/3/find/find-by-id
people: https://developers.themoviedb.org/3/people/get-person-details
multi: https://developers.themoviedb.org/3/search/multi-search

https://reactjs.org/docs/shallow-renderer.html
https://reactjs.org/docs/context.html

## Assignment description
- [ ] A search field.
- [ ] A list of search results. Sorted by relevance.
- [ ] A detail view of a movie including a list of actors. Actors should be linked to a detail
view for actors.
- [ ] A detail view for an actor, including that actors top 10 popular movies.
- [ ] It should be responsive and look good on, at least, desktop, tablet portrait and mobile
portrait.
- [ ] It should support the latest versions of IE, Chrome, Safari and Firefox.
- [ ] JS and CSS should be minified.




