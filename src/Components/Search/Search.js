import React, { Component } from 'react';
import './Search.scss';

class Search extends Component {
  constructor(props){
    super(props)
    this.state = {
      search: ''
    }
    this.handleSearchChange = this.handleSearchChange.bind(this)
  }
  handleSearchChange(event) {
    this.setState({search: event.target.value})
  }
  render() {
    return (
      <div className="tile search">
        <input
          type="search"
          className="input"
          id="movie-search"
          name="q"
          aria-label="Search for anything"
          onChange={this.handleSearchChange}
          value={this.state.search} />
        <button className="button is-primary" onClick={() => this.props.onSearch(this.state.search)}>Search</button>
      </div>
    )
  }
}

export default Search;