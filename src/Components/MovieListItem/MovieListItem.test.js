import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import renderer from 'react-test-renderer';
import MovieListItem from './MovieListItem';


test('render image', () => {
  let movie = {
    poster_path: '/image.png',
    title: 'title',
    overview: 'overview'
  }
  const component = renderer.create(
    <MovieListItem {...movie} />,
  );
  let tree = component.toJSON();
  let image = tree.children[0].children[0].children[0].children[0].props.src
  expect(image).toBe('https://image.tmdb.org/t/p/w200/image.png')
  expect(tree).toMatchSnapshot();
});
