import React, { Component } from 'react';
import './MovieListItem.scss';
import config from './../../config/';
const {movie_image_url} = config;

class MovieListItem extends Component {
  render() {
    let imageUrl = 'https://picsum.photos/200/300'
    if (this.props.poster_path) {
      imageUrl = `${movie_image_url}/w200${this.props.poster_path}`;
    }
    return (
      <div className="movie-list-item column is-one-third">
        <div className="card">
          <div className="card-image">
            <figure className="image is-4by3">
              <img src={imageUrl} alt={this.props.title} />
            </figure>
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-content">
                <p className="title is-4">{this.props.title}</p>
              </div>
            </div>
            <div className="content movie-list-item-content">
              {this.props.overview}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MovieListItem;