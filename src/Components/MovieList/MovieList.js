import React, { Component } from 'react';
import MovieListItem from './../MovieListItem/MovieListItem';
import './MovieList.scss';

class MovieList extends Component {
  render() {
    const movieList = this.props.movies.map((item, key) => <MovieListItem key={key} {...item} /> )
    return (
      <div className="columns movie-list">
        {movieList}
      </div>
    )
  }
}

export default MovieList;