import React, { Component } from 'react';
import axios from 'axios';

import './App.scss';
import MovieListView from './View/MovieListView/MovieListView';
import MovieList from './Components/MovieList/MovieList';
import config from './config';
import Search from './Components/Search/Search';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      movies: [],
      movie: null,
      showMovie: false,
      movieSearchView: true,
      showActor: false,
    }
    this.setStateFromChild = this.setStateFromChild.bind(this)
    this.searchForMovie = this.searchForMovie.bind(this)
  }

  searchForMovie(searchString) {
    axios.get(`${config.movie_api_url}/search/movie`, {
      params: {
        api_key: config.movie_api_key,
        language: 'en-US',
        query: searchString,
        page: 1,
        include_adult: false
      }
    })
      .then((res) => this.setState({ movies: res.data.results }))
      .catch((error) => console.log(error))
  }

  setStateFromChild(state) {
    this.setState(state)
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Movie night!</h1>
        </header>
        {this.state.movieSearchView ?
          <MovieListView>
            <Search onSearch={this.searchForMovie}/>
            <MovieList
              setStateFromChild={this.setStateFromChild}
              movies={this.state.movies} />
           </MovieListView>
          : null}
        {this.state.showActor ? <div></div> : null}
        {this.state.showMovie ? <div></div> : null}
      </div>
    );
  }
}

export default App;