import React, { Component } from 'react';
import './MovieListView.scss';

class MovieListView extends Component {
  render() {
    return (
    <section className="section">
      <div className="container">
        {this.props.children}
      </div>
    </section>
    )
  }
}

export default MovieListView;