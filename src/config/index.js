const env = process.NODE_ENV || 'development'

const config = require(`./env.${env}`).default
export default config
